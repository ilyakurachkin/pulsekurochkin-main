import Foundation
import UserNotifications


class AssistantSingleton {
    static let shared = AssistantSingleton()

    let userDefaults = UserDefaults.standard
    private let selectedDateKey = "SelectedDate"
   
    
    var selectedDate: Date? {
        get {
            return userDefaults.object(forKey: selectedDateKey) as? Date
        }
        set {
            userDefaults.set(newValue, forKey: selectedDateKey)
            removeNotification()
            addNotification()
            NotificationCenter.default.post(name: Notification.Name("DataPickerTime"), object: nil)
        }
    }
    
    
    private init() {}

    func addNotification() {
        notifRequest {
            let content = UNMutableNotificationContent()
            content.title = "Reminder Title"
            content.body = "Давай мы ждем"
            content.sound = UNNotificationSound.default
            content.badge = 1


            if let selectedDate = self.selectedDate {
                let calendar = Calendar.current
                let dateComponents = calendar.dateComponents([.hour, .minute], from: selectedDate)
                let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)


                let request = UNNotificationRequest(identifier: "Reminder", content: content, trigger: trigger)

                UNUserNotificationCenter.current().add(request) { (error) in
                    print("req = ", request)
                    print("errnil = ", error as Any)
                    if let error = error {
                        print("Error \(error.localizedDescription)")
                    }
                }
            }
        }
    }

    func removeNotification() {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.removePendingNotificationRequests(withIdentifiers: ["Reminder"])
    }

    func notifRequest(complete: @escaping (() -> Void)) {
        let notificationCenter = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        notificationCenter.requestAuthorization(options: options) { (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            } else {
                DispatchQueue.main.async {
                    complete()
                }
            }
        }
    }
    func clearSelectedDate() {
        userDefaults.removeObject(forKey: selectedDateKey)
    }
}

 
