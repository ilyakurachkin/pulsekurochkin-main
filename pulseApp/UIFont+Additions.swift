//  UIFont+Additions.swift
//  Best Apps - Tonome
//
//  Generated on Zeplin. (01.04.2020).
//  Copyright (c) 2020 __MyCompanyName__. All rights reserved.


import UIKit

extension UIFont {

  class var sfProRoundedMedium: UIFont {
    return UIFont(name: "SFProRounded-Medium", size: 40.0)!
  }

  class var newYorkExtraLargeBold: UIFont {
    return UIFont(name: "NewYorkExtraLarge-Bold", size: 34.0)!
  }

  class var newYorkBoldMedium: UIFont {
    return UIFont(name: "NewYorkMedium-Bold", size: 34.0)!
  }

  class var sfProRoundedSemibold: UIFont {
    return UIFont(name: "SFProRounded-Semibold", size: 28.0)!
  }

  class var sfProDisplayLight: UIFont {
    return UIFont.systemFont(ofSize: 24.0, weight: .light)
  }

  class var sfProRoundedBold: UIFont {
    return UIFont(name: "SFProRounded-Bold", size: 17.0)!
  }

  class var sfProRoundedRegular: UIFont {
    return UIFont(name: "SFProRounded-Regular", size: 17.0)!
  }

  class var sfProTextMedium: UIFont {
    return UIFont.systemFont(ofSize: 14.0, weight: .medium)
  }

}
