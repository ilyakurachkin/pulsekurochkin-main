//
//  HealthTableViewCell.swift
//  pulseApp
//
//  Created by Илья Курочкин on 25.05.23.


import UIKit

class HealthTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellView: HealthCellView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellView.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
