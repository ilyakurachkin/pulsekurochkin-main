//
//  HealthCellView.swift
//  pulseApp
//
//  Created by Илья Курочкин on 25.05.23.
//

import Foundation
import UIKit

class HealthCellView:UIView{
    
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var healthCellAddLabel: UILabel!
    @IBOutlet weak var healthCellLabel: UILabel!
    @IBOutlet weak var healthCellImage: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    
    
    func customInit() {
        Bundle.main.loadNibNamed("HealthCellView", owner: self, options: nil)
        addSubview(contentView)
        self.backgroundColor = UIColor(red: 0.129, green: 0.133, blue: 0.149, alpha: 1)
        contentView.frame = self.bounds
        contentView.backgroundColor = .clear
        healthCellLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.75)
        NotificationCenter.default.addObserver(self, selector: #selector(updateSelectedValue(_:)), name: Notification.Name("SelectedValueNotification"), object: nil)
        
        
    }
    @objc func updateSelectedValue(_ notification: Notification) {
            if let selectedValue = notification.object as? String {
            healthCellAddLabel.text = selectedValue
        }
        
    }
   
}

