//
//  ViewController.swift
//  pulseApp
//
//  Created by Иван петрашко on 29.03.23.
//

import UIKit

enum ViewControllerType{
    case bannerTable
    case table1
    case table2
    case title(String)
    case margin
}


class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    
    var arrTable:[ViewControllerType] = [.bannerTable,.title("PROFILE"),.table1,.margin,.title("APPLICATION"),.table2]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTable.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
     
        switch arrTable[indexPath.row] {
            
        case .margin:
            let cell = table.dequeueReusableCell(withIdentifier: "MarginCell", for: indexPath) as! MarginCell
            
            
            return cell
            
        case .bannerTable:
            let cell = table.dequeueReusableCell(withIdentifier: "HeartMonitorCell", for: indexPath) as! HeartMonitorCell
            
            return cell
            
        case .table1:
            let cell = table.dequeueReusableCell(withIdentifier: "SettingMainTableCell", for: indexPath) as! SettingMainTableCell
            cell.table.arrTable = [.health,.redminer]
            
            cell.table.firstTableView = tableView
            
            return cell
            
        case .table2:
            let cell = table.dequeueReusableCell(withIdentifier: "SettingMainTableCell", for: indexPath) as! SettingMainTableCell
            cell.table.arrTable = [.rateUs,.hoveTechnology,.contactUs,.privacyPolicy,.termsofUse]
            return cell
            
        case .title(let name):
            let cell = table.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleCell
            cell.label.text = name
            return cell
        }
        
    }
    
    var banner = true
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch arrTable[indexPath.row] {
           case .bannerTable:
               banner = !banner
               reloadBanner()
           default:
               break
           }
        }
    
    
    internal func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch arrTable[indexPath.row] {
        case .bannerTable:
            return 128
        case .table1:
            return 128
        case .table2:
            return 320
        case .margin:
            return 16
        case .title(_):
            return 60
        }
        
    }
    func reloadBanner(){
        if banner {
            arrTable = [.bannerTable,.title("PROFILE"),.table1,.margin,.title("APPLICATION"),.table2]
        } else {
            arrTable = [.title("PROFILE"),.table1,.margin,.title("APPLICATION"),.table2]
        }
       
        self.table.reloadData()
    }
    
    
    @objc func closeButtonTapped() {
       
        dismiss(animated: true, completion: nil)
    }
   
    
    @IBOutlet var colorView: UIView!
    @IBOutlet weak var table: UITableView!


    override func viewDidLoad() {
        
        
        let closeButton = UIBarButtonItem(image: UIImage(named: "xmark"), style: .plain, target: self, action: #selector(closeButtonTapped))
        self.navigationItem.rightBarButtonItem = closeButton
        closeButton.tintColor = .white
        closeButton.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        
        
        var settingTableView: SettingTableView!
        super.viewDidLoad()
        table.dataSource = self
        table.delegate = self
        table.showsVerticalScrollIndicator = false
        table.clipsToBounds = true
        colorView.backgroundColor = UIColor(red: 0.118, green: 0.118, blue: 0.118, alpha: 1)
        table.cellLayoutMarginsFollowReadableWidth = false
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
       
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadDataNotification(_:)), name: Notification.Name("DataPickerTime"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification(_:)), name: Notification.Name("SettingTableViewTypeSelected"), object: nil)
                settingTableView = SettingTableView()
                settingTableView.parentViewController = self
    }
    
    
    @objc func reloadDataNotification(_ notification: Notification) {
        self.table.reloadData()
    }
    
    @objc func handleNotification(_ notification: Notification) {
        guard let arrya = notification.userInfo?["type"] as? SettingTableViewType else {
             return
         }
            
        switch arrya {
        
        case.health:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
          let VC = storyboard.instantiateViewController(withIdentifier: "HealthDetailsViewController") as! HealthDetailsViewController
            navigationController?.pushViewController(VC, animated: true)
            
        case.redminer:
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let VC = storyboard.instantiateViewController(withIdentifier: "ReminderViewController") as! ReminderViewController
            navigationController?.pushViewController( VC, animated: true)
        default:
            break
        }
        
           print("Принимаю уведомление в ViewController: \(arrya)")
    }
}




