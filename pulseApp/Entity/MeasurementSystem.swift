//
//  MeasurementSystem.swift
//  pulseApp
//
//  Created by Иван петрашко on 20.06.23.
//

import Foundation

enum MeasurementSystem: Int {
    case metric = 0 // Метрическая система (kg, cm, m)
    case imperial = 1 // Имперская система (lb, in, ft)
    
    
    
    static func getStandart() -> MeasurementSystem {
        MeasurementSystem.init(rawValue: UserDefaults.standard.integer(forKey: "MeasurementSystem")) ?? .metric
    }
    
    static func setStandart(system: MeasurementSystem) {
        UserDefaults.standard.set(system.rawValue, forKey: "MeasurementSystem")
    }
    
    func getWeightText()->String{
        if self == .metric {
            return "kg"
        } else {
            return "lb"
        }
    }
    func getWeightTextLarge()->String{
        if self == .metric{
            return "%@ kg"
        } else {
            return "%@ lb"
        }
    }
    
    func getAgeTextLarge()->String{
        return  "%@ years"
    }
    
    func getHeightTextLarge()->String{
        if self == .metric{
            return "%@ m %@ cm"
        } else {
            return "%@ ft %@ in"
        }
    }
    
    
    func getHeightTextCmIn()->String {
        if self == .metric {
            return "cm"
        } else {
            return "in"
        }
    }
    
    func getHeightTextFtM()->String {
        if self == .metric {
            return "m"
        } else {
            return "ft"
        }
    }
    
  
}




