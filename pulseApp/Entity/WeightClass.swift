//
//  Weight.swift
//  pulseApp
//
//  Created by Илья Курочкин on 20.06.23.
//

import Foundation

class Weight {

    var kilograms: Double = 0.0

    var kilogramsInt: Int {
        return Int(kilograms)
    }
    init(kilograms: Double) {
        self.kilograms = Double(kilograms)
    }
    init(foont: Double) {
        self.kilograms = Double(foont) * 0.45359237
    }
    
    func getValue(for system: MeasurementSystem) -> String {
        switch system {
        case .metric:
            return String(format: system.getWeightTextLarge(), "\(kilogramsInt)")
        case .imperial:
            let foont = convertToImperial()
            return String(format: system.getWeightTextLarge(), "\(foont)")
        }
    }
    func valueIsNull()->Bool{
              if kilograms == 0 {
                  return true
              } else {
                  return false
              }
          }

    func convertToImperial() -> Int {
        let foont = Double(kilograms) * 2.20462
        return Int(Double(foont).rounded())
    }
        static func getStandart()->Weight {
        return  Weight.init(kilograms: UserDefaults.standard.double(forKey: "Weight"))
    }
        static func setStandart(value:Weight){
        UserDefaults.standard.set(value.kilograms, forKey: "Weight")
    }
}
