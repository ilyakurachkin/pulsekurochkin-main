////
////  Gender.swift
////  pulseApp
////
////  Created by Илья Курочкин on 20.06.23.
////

import Foundation

class Gender {
   static let maleString = "Male"
   static let femaleString = "Female"

    var gender: String?

    init(gender: String?) {
        self.gender = gender
    }


    func valueIsNil() -> Bool {
        return gender == nil
    }

    static func getStandard() -> Gender? {
        let genderString = UserDefaults.standard.string(forKey: "Gender")
        return Gender(gender: genderString)
    }

    static func setStandard(value: Gender?) {
        UserDefaults.standard.set(value?.gender, forKey: "Gender")
    }
}
