//
//  Age.swift
//  pulseApp
//
//  Created by Илья Курочкин on 20.06.23.
//

import Foundation


class Age {
        
    var age: Int = 0
    
    static let ageString = "years"
    
    init(age: Int) {
        self.age = age
    }
    
    func valueIsNull() -> Bool{
              if age == 0 {
                  return true
              } else {
                  return false
              }
          }
    static func getStandart()->Age {
        return Age.init(age: UserDefaults.standard.integer(forKey: "Age"))
}
    static func setStandart(value:Age){
    UserDefaults.standard.set(value.age, forKey: "Age")
}
}
