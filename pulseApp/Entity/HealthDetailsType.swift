//
//  HealthDetailsType.swift
//  pulseApp
//
//  Created by Илья Курочкин on 20.06.23.
//

import Foundation

enum HealthTableType{
    case Gender
    case Height
    case Weight
    case Age

    func getTitle() -> String {
        switch self {
        case .Gender:
            return "Gender"
        case .Height:
            return "Height"
        case .Weight:
            return "Weight"
        case .Age:
            return "Age"
        }
    }

    func getImage() -> String {
        switch self {
        case .Gender:
            return "Gender"
        case .Height:
            return "Height"
        case .Weight:
            return "Weight"
        case .Age:
            return "Age"
        }
    }

}
