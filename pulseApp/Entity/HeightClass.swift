//
//  File.swift
//  pulseApp
//
//  Created by Илья Курочкин on 20.06.23.
//

import Foundation

class Height {

     var meters: Double
     var centimeters: Double

    init(meters: Double, centimeters: Double) {
        self.meters = Double(meters)
        self.centimeters = Double(centimeters)
    }
    var heightIntMeters: Int {
        return Int(meters)
    }
    var heightIntCentimeters: Int {
        return Int(centimeters)
    }
    init(feet: Double, inches: Double) {
        let totalInches = (feet * 12) + inches
        let totalCentimeters = Double(totalInches) * 2.54
        self.meters = Double(Int(totalCentimeters / 100))
        self.centimeters = Double(ceil(totalCentimeters.truncatingRemainder(dividingBy: 100)))
    }
    var feet: Double {
        return Double(convertToImperial().feet)
     }

     var inches: Double {
         return Double(convertToImperial().inches)
     }

    func valueIsNull()->Bool{
              if heightIntMeters == 0 && heightIntCentimeters == 0{
                  return true
              } else {
                  return false
              }
          }

    func getImperialValues() -> (feet: Int, inches: Int) {
           let imperialValues = convertToImperial()
           let feet: Int
           let inches: Int
           
           if meters < 1 || (meters == 1 && centimeters < 22) {
               feet = 4
               inches = 0
           } else {
               feet = imperialValues.feet
               inches = imperialValues.inches
           }
           
           return (feet, inches)
       }
    
    func getValue(for system: MeasurementSystem) -> String {
        let addText = "Add"
        if heightIntMeters == 0 && heightIntCentimeters == 0 {
            return addText
        }
        switch system {
        case .metric:
        return String(format: system.getHeightTextLarge(), "\(heightIntMeters)","\(heightIntCentimeters)" )
                   
        case .imperial:
           
            let imperialValues = convertToImperial()
            let feet: Int
            let inches: Int


            if meters < 1 || (meters == 1 && centimeters < 22) {
                feet = 4
                inches = 0
            } else {
                feet = imperialValues.feet
                inches = imperialValues.inches
            }
//            return "\(feet) ft \(inches) in"
            return String(format: system.getHeightTextLarge(), "\(feet)","\(inches)" )
        }
    }
    
    func convertToImperial() -> (feet: Int, inches: Int) {
        let totalCentimeters = (meters * 100) + centimeters
        let totalInches = Int(Double(totalCentimeters) / 2.54)
        let feet = totalInches / 12
        let inches = totalInches % 12
        return (feet, inches)
    }

    static func getStandart() -> Height {
        return Height.init(meters: Double(UserDefaults.standard.integer(forKey: "HeightMeters")),
                         centimeters: Double(UserDefaults.standard.integer(forKey: "HeightCentimeters")))
    }

    static func setStandart(value: Height) {
        UserDefaults.standard.set(value.meters, forKey: "HeightMeters")
        UserDefaults.standard.set(value.centimeters, forKey: "HeightCentimeters")
    }

}

