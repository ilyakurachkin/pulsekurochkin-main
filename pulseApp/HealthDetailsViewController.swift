    //
    //  HealthDetailsViewController.swift
    //  pulseApp
    //
    //  Created by Илья Курочкин on 24.05.23.
    //

    import Foundation
    import UIKit


    class HealthDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

        @IBOutlet weak var healthSegmentedControl: UISegmentedControl!
        @IBOutlet weak var table: UITableView!
        @IBOutlet weak var healthBgrView: UIView!
        @IBOutlet weak var applyStackViewButton: UIButton!
        @IBOutlet weak var stackViewLabel: UILabel!
        @IBOutlet weak var healthStackViewVertical: UIStackView!
        @IBOutlet weak var cancelStackViewButton: UIButton!
        @IBOutlet weak var healthViewPicker: UIPickerView!

        var selectedValueNotification: NSObjectProtocol?
        var arrayHealth:[HealthTableType] = [.Gender,.Height,.Weight,.Age]
        var selectedPickerValueColor: UIColor = .white
        var currentMeasurementSystem: MeasurementSystem = .metric
        var currentPickerType: HealthTableType?
        

        override func viewDidLoad() {
            super.viewDidLoad()

            healthSegmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)
            healthViewPicker.dataSource = self
            healthViewPicker.delegate = self

            table.separatorColor = UIColor .clear
            table.dataSource = self
            table.delegate = self
            table.isScrollEnabled = false
            healthBgrView.backgroundColor = UIColor(red: 0.118, green: 0.118, blue: 0.118, alpha: 1)
            navigationController?.navigationBar.tintColor = .white
            healthViewPicker.backgroundColor = UIColor(red: 0.129, green: 0.133, blue: 0.149, alpha: 1)
            healthStackViewVertical.isHidden = true
            healthSegmentedControl.selectedSegmentIndex = MeasurementSystem.getStandart().rawValue
                }

        private func setInitialPickerValues() {
            let feet = Height.getStandart().getImperialValues().feet
            let inches = Height.getStandart().getImperialValues().inches
            healthViewPicker.selectRow(feet , inComponent: 0, animated: false)
            healthViewPicker.selectRow(inches, inComponent: 2, animated: false)
            }
        
        private func heightMetricPickerValues(){
            healthViewPicker.selectRow(Int(Height.getStandart().meters) - 1, inComponent: 0, animated: false)
            healthViewPicker.selectRow(Int(Height.getStandart().centimeters), inComponent: 2, animated: false)
        }
        @IBAction func segmentedControlValueChanged(_ sender: UISegmentedControl) {

            MeasurementSystem.setStandart(system: MeasurementSystem(rawValue: healthSegmentedControl.selectedSegmentIndex)!)

            healthViewPicker.reloadAllComponents()

            switch currentPickerType {

            case .Gender:
                break
            case .Height:
                if MeasurementSystem.getStandart() == .imperial {
                    setInitialPickerValues()
                } else {
                    heightMetricPickerValues()
                }
            case .Weight:
                if MeasurementSystem.getStandart() == .imperial {
                    healthViewPicker.selectRow(Int(Double(Weight.getStandart().convertToImperial())), inComponent: 0, animated: false)
                } else {
                    healthViewPicker.selectRow(Int(Double(Weight.getStandart().kilograms)), inComponent: 0, animated: false)
                }
            case .Age:
                break
            case .none:
                break
            }
            self.table.reloadData()
        }
        @IBAction func cancelStackViewButtonTapped(_ sender: UIButton) {
            healthStackViewVertical.isHidden = true
        }
        @IBAction func applyStackViewButtonTapped(_ sender: UIButton) {

                switch currentPickerType  {

                case .Height:
                    let selectedRow1 = healthViewPicker.selectedRow(inComponent: 0)
                    let selectedRow2 = healthViewPicker.selectedRow(inComponent: 2)
                    var height = Height(meters: Double(selectedRow1 + 1), centimeters: Double(selectedRow2))

                    if MeasurementSystem.getStandart() == .imperial {
                        height = Height(feet: Double(selectedRow1), inches: Double(selectedRow2))
                    }
                    Height.setStandart(value: height)
                    self.table.reloadData()

                case .Weight:

                    let selectedRow = healthViewPicker.selectedRow(inComponent: 0)
                    var weight = Weight(kilograms: Double(selectedRow))
                    
                    if MeasurementSystem.getStandart() == .imperial {
                        weight = Weight(foont: Double(selectedRow))
                    }
                    Weight.setStandart(value: weight)
                    self.table.reloadData()
                    
                case .Gender:
                    let selectedRow = healthViewPicker.selectedRow(inComponent: 0)
                    let gender = selectedRow == 0 ? Gender.maleString : Gender.femaleString
                    let genderObject = Gender(gender: gender)
                    Gender.setStandard(value: genderObject)
                    self.table.reloadData()

                case .Age:
                    let selectedRow = healthViewPicker.selectedRow(inComponent: 0)
                    let age = Age(age: Int(selectedRow))
                    
                    Age.setStandart(value: age)
                    self.table.reloadData()
                case .none:
                    break
                }
            NotificationCenter.default.post(name: Notification.Name("SelectedValueNotification"), object: nil)
        }

        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 68
        }

        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return arrayHealth.count
        }

        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: HealthTableViewCell.identifier, for: indexPath) as? HealthTableViewCell else {
                fatalError("Не удалось получить ячейку SettingTableCell")
            }
            let tableType = arrayHealth[indexPath.row]

                   cell.cellView.healthCellLabel.text = tableType.getTitle()
                   cell.cellView.healthCellImage.image = UIImage(named: tableType.getImage())
                   cell.cellView.healthCellAddLabel.textColor = UIColor(red: 0.442, green: 0.398, blue: 0.938, alpha: 1)

            switch tableType {

            case .Gender:
                if !Gender.getStandard()!.valueIsNil() {
                    cell.cellView.healthCellAddLabel.text = Gender.getStandard()?.gender
                    cell.cellView.healthCellAddLabel.textColor = .white
                } else {
                    cell.cellView.healthCellAddLabel.text = "Add"
                }
            case .Height:
                let measurementSystem = MeasurementSystem.getStandart()
               if Height.getStandart().valueIsNull(){
                   cell.cellView.healthCellAddLabel.text = "Add"
               } else {
                   cell.cellView.healthCellAddLabel.textColor = .white
                   cell.cellView.healthCellAddLabel.text = Height.getStandart().getValue(for: measurementSystem)
               }
            case .Weight:
                let measurementSystem = MeasurementSystem.getStandart()
                if Weight.getStandart().valueIsNull() {
                    cell.cellView.healthCellAddLabel.text = "Add"
                } else {
                    cell.cellView.healthCellAddLabel.textColor = .white
                    cell.cellView.healthCellAddLabel.text = Weight.getStandart().getValue(for: measurementSystem)
                }
            case .Age:
                if !Age.getStandart().valueIsNull() {
                    cell.cellView.healthCellAddLabel.textColor = .white
                    cell.cellView.healthCellAddLabel.text = String(Age.getStandart().age)
                } else {
                    cell.cellView.healthCellAddLabel.text = "Add"
                }
            }
                     return cell
            }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
                  tableView.deselectRow(at: indexPath, animated: true)

                  let tableType = arrayHealth[indexPath.row]
                  currentPickerType = tableType
                  healthViewPicker.reloadAllComponents()
                  
            switch currentPickerType {
                
            case .Gender:
                healthViewPicker.selectRow(Gender.getStandard()?.gender == Gender.maleString ? 0 : 1, inComponent: 0, animated: false)
            case .Height:
                    heightMetricPickerValues()
                if MeasurementSystem.getStandart() == .imperial {
                    setInitialPickerValues()
                }
            case .Weight:
                healthViewPicker.selectRow(Int(Weight.getStandart().kilograms), inComponent: 0, animated: false)
                if MeasurementSystem.getStandart() == .imperial {
                    healthViewPicker.selectRow(Int(Weight.getStandart().convertToImperial()), inComponent: 0, animated: false)
                }
            case .Age:
                healthViewPicker.selectRow(Age.getStandart().age, inComponent: 0, animated: false)
            case .none:
                break
            }
                  healthStackViewVertical.isHidden = false
                  stackViewLabel.text = tableType.getTitle()
              }
          }

    extension HealthDetailsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            guard let currentPickerType = currentPickerType else {
                return 1
            }
            if currentPickerType == .Gender {
                return 1
            } else if currentPickerType == .Height {
                return 4
            } else if currentPickerType == .Weight {
                return 2
            } else if currentPickerType == .Age {
                return 1
            }
            return 1
        }

        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if currentPickerType == .Gender {
                return 2
            } else if currentPickerType == .Height {
                let measurementSystem = MeasurementSystem.getStandart()
                if component == 0 {
                    if measurementSystem == .metric {
                        return 2
                    } else if measurementSystem == .imperial {
                        return 11
                    }
                } else if component == 1 {
                    return 1
                } else if component == 2 {
                    if measurementSystem == .metric {
                        return 100
                    } else if measurementSystem == .imperial {
                        return 12
                    }
                } else if component == 3 {
                    return 1
                }
            } else if currentPickerType == .Weight {
                if component == 0 {
                    return 181
                } else if component == 1 {
                    return 1
                }
            } else if currentPickerType == .Age {
                return 91
            }

            return 0
        }

        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            let measurementSystem = MeasurementSystem.getStandart()

            if currentPickerType == .Gender {
                return row == 0 ? Gender.maleString : Gender.femaleString

            } else if currentPickerType == .Height {
                if component == 0 {
                    if measurementSystem == .metric {
                        let meterValue = row + 1
                        return "\(meterValue)"
                    } else if measurementSystem == .imperial {
                        let feetValue = row
                        return "\(feetValue)"
                    }
                } else if component == 1 {
                    return measurementSystem.getHeightTextFtM()
                } else if component == 2 {
                    if measurementSystem == .metric {
                        let centimeterValue = row
                        return "\(centimeterValue)"
                    } else if measurementSystem == .imperial {
                            let imperialValue = row
                            return "\(imperialValue)"
                        }
                    } else if component == 3 {
                        return measurementSystem.getHeightTextCmIn()
                    }
                } else if currentPickerType == .Weight {
                    if component == 0 {
                        return "\(row)"
                    } else if component == 1 {
                        return measurementSystem.getWeightText()
                    }
                } else if currentPickerType == .Age {
                    let age = row
                    return String(format: MeasurementSystem.getStandart().getAgeTextLarge(), "\(age)")
                }
            return nil
        }
        func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

                let label = UILabel()
                label.textColor = .white
                label.textAlignment = .center
                label.font = UIFont.systemFont(ofSize: 25.0)
                label.text = pickerView.delegate?.pickerView?(pickerView, titleForRow: row, forComponent: component)
                return label
            }
        }




