
import UIKit

class FirstViewController: UIViewController {

  
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func openButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC = storyboard.instantiateViewController(withIdentifier:"ViewController") as! ViewController
        let vc = UINavigationController(rootViewController: rootVC)
        vc.modalPresentationStyle = .fullScreen
        vc.navigationController?.isNavigationBarHidden = true
        vc.navigationController?.navigationBar.isTranslucent = true
        present(vc, animated: true)
        }
    }


