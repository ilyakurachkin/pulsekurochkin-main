//
//  marginCell.swift
//  pulseApp
//
//  Created by Илья Курочкин on 29.04.23.
//

import UIKit

class MarginCell: UITableViewCell {
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
