//
//  HeartMonitorCell.swift
//  pulseApp
//
//  Created by Илья Курочкин on 3.04.23.
//

import UIKit

class HeartMonitorCell: UITableViewCell {

    @IBOutlet weak var backgroundImagesHeart: UIImageView!
    @IBOutlet weak var titleHeart: UILabel!
    @IBOutlet weak var subtitleHeart: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()  
        // Initialization code
        self.layer.cornerRadius = 15
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
