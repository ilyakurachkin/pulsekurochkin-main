//
//  SettingMainTableCell.swift
//  pulseApp
//
//  Created by Иван петрашко on 29.03.23.
//

import UIKit

class SettingMainTableCell: UITableViewCell {

    @IBOutlet weak var table: SettingTableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func clearSavedTime() {
        // Удаляем существующий дополнительный лейбл, если он существует
        contentView.subviews.filter { $0.tag == 100 }.forEach { $0.removeFromSuperview() }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
