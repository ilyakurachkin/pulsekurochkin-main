//
//  SettingCellView.swift
//  pulseApp
//
//  Created by Иван петрашко on 29.03.23.
//

import Foundation
import UIKit

class SettingCellView:UIView{
    
    @IBOutlet weak var settingsCellTime: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet var settingCellLabel: UILabel!
    @IBOutlet var settingCellImage: UIImageView!
    
    
    
       required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           customInit()
       }
       
       override init(frame: CGRect) {
           super.init(frame: frame)
           customInit()
       }
       
   
       func customInit() {
           Bundle.main.loadNibNamed("SettingCellView", owner: self, options: nil)
           addSubview(contentView)
           self.backgroundColor = UIColor(red: 0.129, green: 0.133, blue: 0.149, alpha: 1)
           contentView.frame = self.bounds
           contentView.backgroundColor = .clear
       }
}
