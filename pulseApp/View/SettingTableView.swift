//
//  SettingTableView.swift
//  pulseApp
//
//  Created by Иван петрашко on 29.03.23.
//
//
extension UITableViewCell {

  class var identifier: String {
    return String(describing: self)
  }
}

extension UIView {

  class func loadFromNibNamed(_ nibNamed: String, _ bundle: Bundle? = nil) -> UINib {
    return UINib(nibName: nibNamed, bundle: bundle)
  }

  class func loadNib() -> UINib {
    let className = String(describing: self)
    return loadFromNibNamed(className)
  }

}

import Foundation
import UIKit

enum SettingTableViewType{
    
    case health
    case redminer
    case rateUs
    case hoveTechnology
    case contactUs
    case privacyPolicy
    case termsofUse
    
   
    func getTitle() -> String {
        switch self {
        case .health:
            return "Health Details"
        case .redminer:
            return "Reminders"
        case .rateUs:
            return "Rate Us"
        case .hoveTechnology:
            return "How the technology works"
        case .contactUs:
            return "Contact Us"
        case .privacyPolicy:
            return "Privacy Policy"
        case .termsofUse:
            return "Terms of Use"
        }
    }
    
    func getImage() -> String {
        switch self {
        case .health:
            return  "icProfile"
        case .redminer:
            return  "icRemind"
        case .rateUs:
            return  "icRate"
        case .hoveTechnology:
            return  "icTechn"
        case .contactUs:
            return  "icMail"
        case .privacyPolicy:
            return "icSecurity"
        case .termsofUse:
            return "icTerms"
        }
    }


}
    
class SettingTableView:UIView, UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet var contentView: UIView!
    
    func customInit() {
        Bundle.main.loadNibNamed("SettingTableView", owner: self, options: nil)
        addSubview(contentView)
        self.backgroundColor = .clear
        contentView.frame = self.bounds
        table.delegate = self
        table.dataSource = self
        table.register(SettingTableCell.loadNib(), forCellReuseIdentifier: SettingTableCell.identifier)
        
    }
    
    weak var parentViewController: UIViewController?
        
    let userDefaults = AssistantSingleton.shared
    var firstTableView: UITableView?
    
    var arrTable:[SettingTableViewType] = [.health,.redminer,.rateUs,.hoveTechnology,.contactUs,.privacyPolicy,.termsofUse]{
        didSet{
            table.reloadData()
            table.layer.cornerRadius = 15
            table.isScrollEnabled = false
            contentView.backgroundColor = UIColor(red: 0.118, green: 0.118, blue: 0.118, alpha: 1)
            table.separatorColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.1)
            table.separatorStyle = .singleLine
            
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTable.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingTableCell.identifier, for: indexPath) as? SettingTableCell else {
            fatalError("Не удалось получить ячейку SettingTableCell")
        }
        
        let tableType = arrTable[indexPath.row]
        _ = UITableView()
        
        cell.cellview.settingCellLabel.text = tableType.getTitle()
        cell.cellview.settingCellImage.image = UIImage(named: tableType.getImage())
        

        if tableType == .redminer {
            
            cell.cellview.settingsCellTime.isHidden = false

            if let selectedDate = userDefaults.selectedDate {
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm"
                let dateString = formatter.string(from: selectedDate)
                cell.cellview.settingsCellTime.text = dateString
            } else {
                cell.cellview.settingsCellTime.text = nil
            }
        } else {
            cell.cellview.settingsCellTime.isHidden = true
        }
        
        if tableType == .rateUs {
            cell.cellview.settingCellLabel.textColor = UIColor(red: 1.0, green: 0.29, blue: 0.67, alpha: 1)
        } else {
            cell.cellview.settingCellLabel.textColor = UIColor.white
        }
        return cell
    }
    



    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        tableView.beginUpdates()
        //        tableView.endUpdates()
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        let arrya = arrTable[indexPath.row]
        let userInfo = ["type": arrya]
        NotificationCenter.default.post(name: Notification.Name("SettingTableViewTypeSelected"), object: nil, userInfo: userInfo)
        
        print("Нажали в таблице \(arrya) отправили уведомление")
        if let parentViewController = parentViewController as? ReminderViewController {
                parentViewController.selectedIndexPath = indexPath
            }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
}

