//
//  TitleCell.swift
//  pulseApp
//
//  Created by Иван петрашко on 29.03.23.
//

import UIKit

class TitleCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
        label.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.25)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
