//
//  ReminderViewController.swift
//  pulseApp
//
//  Created by Илья Курочкин on 11.05.23.
//

import UIKit
import UserNotifications

class ReminderViewController: UIViewController {
    
    @IBOutlet var bgrReminderView: UIView!
    @IBOutlet weak var redminderImages: UIImageView!
    @IBOutlet weak var timeDatePicker: UIDatePicker!
    @IBOutlet weak var titleReminderLabel: UILabel!
    @IBOutlet weak var changeReminderButton: UIButton!
    @IBOutlet weak var subtitleReminderLabel: UILabel!
    
    let userDefaults = AssistantSingleton.shared
    var selectedIndexPath: IndexPath?
    var lastSelectedDate: Date?
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateButton()
    }
    
    func updateButton(){
        changeReminderButton.titleLabel?.font = UIFont.sfProRoundedBold.withSize(18)
        changeReminderButton.layer.cornerRadius = 20
        if userDefaults.selectedDate != nil {
            changeReminderButton.backgroundColor = UIColor(red: 0.129, green: 0.133, blue: 0.149, alpha: 1)
            changeReminderButton.setTitleColor(UIColor(red: 1, green: 0.292, blue: 0.378, alpha: 1), for: .normal)
            changeReminderButton.setTitle("Turn off reminder", for: .normal)
        } else {
            changeReminderButton.setTitle("Set a Reminder", for: .normal)
            changeReminderButton.setTitleColor(.white, for: .normal)
            changeReminderButton.backgroundColor = UIColor(red: 1, green: 0.292, blue: 0.378, alpha: 1)
        }
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        navigationController?.navigationBar.tintColor = .white
        
        timeDatePicker.addTarget(self, action: #selector(datePickerValueChanged(_:)), for: .valueChanged)
        

        if let selectedDate = userDefaults.selectedDate {
            print("Сохраненная дата: \(selectedDate)")
        } else {
            print("Нет сохраненных данных")
        }
        
        timeDatePicker.datePickerMode = .time
        timeDatePicker.setValue(UIColor.white, forKey: "textColor")
        
        bgrReminderView.backgroundColor = UIColor(red: 0.118, green: 0.118, blue: 0.118, alpha: 1)
        titleReminderLabel.text = "Let’s schedule your daily heart rate reminder"
        titleReminderLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        subtitleReminderLabel.text = "Measuring your heart rate daily lets \n you see trends over time"
        subtitleReminderLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.75)
        loadSelectedDate()
    }
    
   
    @objc func datePickerValueChanged(_ datePicker: UIDatePicker) {
        if userDefaults.selectedDate != nil {
            userDefaults.selectedDate = datePicker.date
        }
    }

    @IBAction func changeReminderButtonTapped(_ sender: UIButton) {
        
        if userDefaults.selectedDate != nil {
            let alertController = UIAlertController(title: "Are you sure?", message: "The heart rate measurement reminder will be disabled", preferredStyle: .alert)
            
               let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
               let deleteAction = UIAlertAction(title: "Turn off", style: .destructive) { (_) in
                   self.deleteSelectedDate()
               }
               
               alertController.addAction(cancelAction)
               alertController.addAction(deleteAction)
               
               present(alertController, animated: true, completion: nil)

        } else {
            saveSelectedDate()

        }
   }
    
    private func deleteSelectedDate() {
        userDefaults.selectedDate = nil
        updateButton()
    }
    
    private func saveSelectedDate() {
        userDefaults.selectedDate = timeDatePicker.date
        updateButton()
    }
    
    
    private func loadSelectedDate() {
        if let selectedDate = userDefaults.selectedDate {
            timeDatePicker.date = selectedDate
            
        }
            else {
            _ = Calendar.current
            _ = Date()

        }
    }
    
}




    
    

